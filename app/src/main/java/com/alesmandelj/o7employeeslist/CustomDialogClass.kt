package com.alesmandelj.o7employeeslist

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Window
import kotlinx.android.synthetic.main.add_employee_dialog.*
import java.text.SimpleDateFormat
import com.alesmandelj.o7employeeslist.MainActivity.EmployeesArrayList.employeeArrayList
import com.alesmandelj.o7employeeslist.MainActivity.EmployeesArrayList.recyclerViewAdapter
import com.parse.ParseObject
import java.text.ParseException


private val TAG="Dialog:"
val dateFormat=SimpleDateFormat("dd/MM/YYYY")

class CustomDialogClass(context: Context):Dialog(context) {
    init {
        setCancelable(true)
    }


    fun updateRecyclerView(){

        recyclerViewAdapter.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.add_employee_dialog)



        buttonCancel.setOnClickListener {
            cancel()
        }

        //Save new employee

        buttonSave.setOnClickListener {
            val gender:String
            val checkedEditTexts = onCheckEditText()

            if (!checkedEditTexts) {

                Log.d(TAG,"Something empty")
            }else{

                gender = if(radioButtonMale.isChecked){
                    "male"
                }else{
                    "female"
                }
                val salary :Double=java.lang.Double.parseDouble(editTextSalary.text.toString())

                //create new employee on parse server

                val employeeParse=ParseObject("Employees")
                val employeeId=(employeeArrayList.get((employeeArrayList.size)-1).id)+1

                employeeParse.put("name","${editTextName.text} ${editTextLastName.text}")
                employeeParse.put("birthDate",editTextBirthdayDate.text.toString())
                employeeParse.put("gender",gender)
                employeeParse.put("salary",salary)
                employeeParse.put("employeeId", employeeId)
                employeeParse.saveInBackground()

                val employee=EmployeeObjectClass("${editTextName.text} ${editTextLastName.text}",
                    employeeId,editTextBirthdayDate.text.toString(),gender,salary)

                Log.d(TAG, "${employee.id}, ${employee.name}, ${employee.dateOfBirth}, ${employee.gender}, ${employee.salary}")

                employeeArrayList.add(employee)

                updateRecyclerView()

                Log.d(TAG,"all good")
                cancel()
            }




        }
    }
    //Error provider-> empty input, wrong format

    fun onCheckEditText(): Boolean {

        var checkEditText = true

        if (TextUtils.isEmpty(editTextName.text.toString())) {
            editTextName.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editTextLastName.text.toString())) {
            editTextLastName.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editTextBirthdayDate.text.toString())) {
            editTextBirthdayDate.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editTextSalary.text.toString())) {
            editTextSalary.error = "No value input"
            checkEditText = false
        }
        try
        {
            dateFormat.parse(editTextBirthdayDate.text.toString())
        } catch (e:ParseException) {
            editTextBirthdayDate.error="Wrong date format dd/MM/YYYY"
            checkEditText=false
            Log.d(TAG,"Date fomat error")
        }


        return checkEditText
    }


}







