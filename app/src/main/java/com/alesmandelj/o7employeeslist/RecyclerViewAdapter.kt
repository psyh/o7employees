package com.alesmandelj.o7employeeslist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

private const val TAG="RecyclerAdapter"

//MainActivity employee_card_view recyclerAdapter

class EmployeeViewHolder(view : View) : RecyclerView.ViewHolder(view){
    var employeeName: TextView =view.findViewById(R.id.textViewEmployeeName)

}

class RecyclerAdapter(private val employeeList:ArrayList<EmployeeObjectClass>): RecyclerView.Adapter<EmployeeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        Log.d(TAG,".onCreateViewHolder new view requested")
        val view= LayoutInflater.from(parent.context).inflate(R.layout.employee_card_view,parent,false)
        return EmployeeViewHolder(view)
    }

    override fun getItemCount(): Int {
        //  Log.d(TAG,".getItemCount called")
        return if (employeeList.isNotEmpty()) employeeList.size else 0
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        if(employeeList.isEmpty()){
            holder.employeeName.text=("")

        }else{
            val employeeItem=employeeList[position]
            holder.employeeName.text=employeeItem.name

        }
    }

}