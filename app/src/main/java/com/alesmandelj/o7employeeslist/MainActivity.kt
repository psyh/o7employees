package com.alesmandelj.o7employeeslist


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), RecyclerItemClickListener.OnRecyclerClickListener {


    val TAG="MainActivity;"
    companion object EmployeesArrayList{
        val employeeArrayList=ArrayList<EmployeeObjectClass>()
        val recyclerViewAdapter =RecyclerAdapter(employeeArrayList)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        getEmployeeArray()

        fab.setOnClickListener { view ->
            val cd1=CustomDialogClass(this)
            cd1.show()

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {

            // Run analytics activity

            R.id.action_analytics -> {
                val intent = Intent(applicationContext, AnalyticsActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //Get employees from parse server and fill employeeArrayList

    private fun getEmployeeArray(){

        val query = ParseQuery<ParseObject>("Employees")
        query.findInBackground { employeeList, e ->
            if (e == null) {

                var employeeObjectItem:EmployeeObjectClass

                for (i in 0 until employeeList.size){
                        employeeObjectItem=EmployeeObjectClass(employeeList[i].get("name").toString(),
                        employeeList[i].get("employeeId").toString().toInt(),
                        employeeList[i].get("birthDate").toString(),
                        employeeList[i].get("gender").toString(),
                        employeeList[i].get("salary").toString().toDouble())

                    employeeArrayList.add(employeeObjectItem)
                    Log.d(TAG,"${employeeObjectItem.name} ,${employeeObjectItem.dateOfBirth}, ${employeeObjectItem.salary}")
                }

                recycler_view_eployees.layoutManager= LinearLayoutManager(this)
                recycler_view_eployees.addOnItemTouchListener(RecyclerItemClickListener(this,recycler_view_eployees,this))
                recycler_view_eployees.adapter=recyclerViewAdapter

            } else {
                Log.d(TAG,"No info to display")
            }
        }
    }

    //Set recyclerView on click listener

    override fun onItemClick(view: View, position: Int) {

            val intentId= employeeArrayList[position].id
            val intent = Intent(applicationContext, GoogleSearchActivity::class.java)
            intent.putExtra("employee", intentId)

        Log.d(TAG,"Intent id: ${employeeArrayList[position].id}")
            startActivity(intent)

    }

    override fun onItemLongClick(view: View, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
