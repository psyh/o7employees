package com.alesmandelj.o7employeeslist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


private val TAG="QueryViewHolder"

//GooGleSearchActivity search_employee_view recyclerAdapter

class FeedViewHolder(view : View) : RecyclerView.ViewHolder(view){

    //Setup CardVIew

    var employeeQueryTitle:TextView=view.findViewById(R.id.textViewSearchEmployee)
    var employeeQuerySnippet:TextView=view.findViewById(R.id.textViewSearchEmployeeSnippet)
    var employeeQuerylink:TextView=view.findViewById(R.id.textViewSearchEmployeeLink)
}
class QueryFeedAdapter(private val employeeQueryList:ArrayList<EmployeeQueryResult>):RecyclerView.Adapter<FeedViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        Log.d(TAG,".onCreateViewHolder new view requested")
        val view= LayoutInflater.from(parent.context).inflate(R.layout.search_employee_view,parent,false)
        return FeedViewHolder(view)
    }

    override fun getItemCount(): Int {
        //  Log.d(TAG,".getItemCount called")
        return employeeQueryList.count()
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {

            val employeeItem=employeeQueryList.get(position)
            holder.employeeQueryTitle.text=employeeItem.title
            holder.employeeQuerySnippet.text=employeeItem.snippet
            holder.employeeQuerylink.text=employeeItem.link
    }
}