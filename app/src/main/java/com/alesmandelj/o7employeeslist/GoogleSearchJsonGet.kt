package com.alesmandelj.o7employeeslist

import android.os.AsyncTask
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import com.alesmandelj.o7employeeslist.GoogleSearchActivity.EmployeeUrl.arrayOfTitlesNew
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_google_search.*
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL




private val TAG="GoogleGetData"



class GoogleSearchJsonGet : AsyncTask<URL, Int, String>() {

    var responseCode=0
    var responseMessage=""
    var result=""

        override fun onPreExecute() {

            Log.d(TAG, "AsyncTask - onPreExecute")
        }

        override fun doInBackground(vararg urls: URL): String? {

            val url = urls[0]
            Log.d(TAG, "AsyncTask - doInBackground, url=$url")

            // Http connection
            var conn: HttpURLConnection? = null
            try {
                conn = url.openConnection() as HttpURLConnection
            } catch (e: IOException) {
                Log.e(TAG, "Http connection ERROR $e")
            }

            try {
                responseCode = conn!!.responseCode
                responseMessage = conn.responseMessage
            } catch (e: IOException) {
                Log.e(TAG, "Http getting response code ERROR $e")

            }

            Log.d(TAG, "Http response code =$responseCode message=$responseMessage")

            // Get raw query data

            try {

                if (responseCode != null && responseCode == 200) {

                    // response OK

                    val rd = BufferedReader(InputStreamReader(conn!!.inputStream))
                    val sb = StringBuilder()
                    var line: String?

                    do {

                        line = rd.readLine()

                        if (line == null)
                            break
                        sb.append(line + "\n")

                    } while (true)

                    rd.close()

                    conn.disconnect()
                    result = sb.toString()

                    return result

                } else {

                    // response problem

                    val errorMsg =
                        "Http ERROR response $responseMessage\nAre you online ? \nMake sure to replace in code your own Google API key and Search Engine ID"
                    Log.e(TAG, errorMsg)
                    result = errorMsg
                    return result
                }

            } catch (e: IOException) {
                Log.e(TAG, "Http Response ERROR $e")
            }

            return null
        }

        override fun onPostExecute(result: String)  {

           // Create Json object

            arrayOfTitlesNew.clear()

            try {
                val jsonData = JSONObject(result)
                val itemsArray=jsonData.getJSONArray("items")

                //Get data from JSON array

                for (i in 5 until itemsArray.length()){
                    val jsonObject=itemsArray.getJSONObject(i)
                    val title=jsonObject.getString("title")
                    val snippet=jsonObject.getString("snippet")
                    val link=jsonObject.getString("link")

                    //Fill arrayOfTitlesNew with title, snippet and link

                    val queryObject=EmployeeQueryResult(title,snippet,link)
                    arrayOfTitlesNew.add(queryObject)
                    Log.d(TAG,"jsonObject title: ${queryObject.title} \r\n snippet: ${queryObject.snippet}\r\n link: ${queryObject.link}")
                }

            }catch (e: JSONException) {
                e.printStackTrace()
                Log.e(TAG, ".doInBackground: Error processing Json data ${e.message}")
                cancel(true)

            }

            updateRecyclerView()

        }

         fun updateRecyclerView(){

             GoogleSearchActivity.recyclerSearchViewAdapter.notifyDataSetChanged()
         }
    }

class EmployeeQueryResult(val title:String,val snippet:String,val link :String)






