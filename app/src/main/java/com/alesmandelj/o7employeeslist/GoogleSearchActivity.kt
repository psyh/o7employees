package com.alesmandelj.o7employeeslist

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.alesmandelj.o7employeeslist.MainActivity.EmployeesArrayList.employeeArrayList
import kotlinx.android.synthetic.main.activity_google_search.*
import java.net.MalformedURLException
import java.net.URL

private val TAG="GoogleSearchActivity"

class GoogleSearchActivity : AppCompatActivity() {



companion object EmployeeUrl{
    var searchString=""
    var arrayOfTitlesNew=ArrayList<EmployeeQueryResult>()
    val recyclerSearchViewAdapter =QueryFeedAdapter(arrayOfTitlesNew)
}

    override fun onBackPressed() {
            finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_search)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Employee info"
        //set back button

        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val employeeObjectId=intent.getIntExtra("employee",0)
        val employeeObject=employeeArrayList[employeeObjectId-1]

        //Employee card info

        textViewSearchName.text="Name: "+employeeObject.name
        textViewSearchBirthday.text="Birth date: "+employeeObject.dateOfBirth
        textViewSearchGender.text="Gender: "+employeeObject.gender
        textViewSearchId.text="Employee Id: "+employeeObject.id.toString()
        textViewSearchSalary.text="Salary: ${employeeObject.salary} €"

        //Search google

        searchString=employeeObject.name

        // remove spaces
        val searchStringNoSpaces = searchString.replace(" ", "+")

        // Google API key
        val key = "AIzaSyAYC_et9R_hCY7LRCA3VkGwYYlwNswPbTw"

        // Google Search Engine ID
        val cx = "012340625599793229455:sxz1zbnhkly"

        val urlString =
            "https://www.googleapis.com/customsearch/v1?q=$searchStringNoSpaces&key=$key&cx=$cx&alt=json"
        var url: URL? = null


        try {
            url = URL(urlString)
        } catch (e: MalformedURLException) {
            Log.e(TAG, "ERROR converting String to URL $e")
        }

        Log.d(TAG, "Url = $urlString")

        val searchObject=GoogleSearchJsonGet()
        searchObject.execute(url)

        recycler_view_google.layoutManager= LinearLayoutManager(this)

        recycler_view_google.adapter= recyclerSearchViewAdapter

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
