package com.alesmandelj.o7employeeslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.alesmandelj.o7employeeslist.MainActivity.EmployeesArrayList.employeeArrayList
import kotlinx.android.synthetic.main.activity_analytics.*

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class AnalyticsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_analytics)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Employee info"
        //set back button

        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        maxSalary()
        getGenderRatio()
        getAvgAge()
    }

    //Calculate max salary
    fun maxSalary(){

        var maxSalary=0.0
        var nameString=""
        for(elements in employeeArrayList){

            if(elements.salary>maxSalary){

                maxSalary=elements.salary
                nameString=elements.name
            }
        }

        textViewMaxSalary.text="Max salary: $maxSalary € earned by: $nameString"
    }

    //Calculate gender ratio

    fun getGenderRatio(){

        var males=0
        var females=0
        var malePercentage=0
        var femalePercentage=0

        for (elements in employeeArrayList){

            if(elements.gender.equals("male")){
                males++
            }else{
                females++
            }
        }

        malePercentage=((males.toDouble()/(males+females))*100).roundToInt()
        femalePercentage=((females.toDouble()/(males+females))*100).roundToInt()

        textViewGenderRatio.text="Males: $males -> Male percentage: $malePercentage %"
        textViewGenderRatio2.text="Females: $females -> Female percentage: $femalePercentage %  "
    }

    //Calculate average age and median age

    fun getAvgAge(){

        var age:Int
        val ageArrayList=ArrayList<Int>()


            for (elements in employeeArrayList){

                var stringArray=elements.dateOfBirth.split("/")


                age=getAge(stringArray[2].toInt(),stringArray[1].toInt(),stringArray[0].toInt())
                ageArrayList.add(age)
                Log.d("Date: ","Current Age $age")
            }

        var ageTotal=0
        var ageMin= ageArrayList[0]
        var ageMax=0
        for (elements in ageArrayList){

            ageTotal+=elements

            if (ageMin>=elements){
                ageMin=elements
            }
            if (ageMax<=elements){
                ageMax=elements
            }
        }

        val ageMedian=((ageMin.toDouble()+ageMax)/2).roundToInt()
        val ageAverage=(ageTotal.toDouble()/ageArrayList.size).roundToInt()

        textViewAveageAge.text="Average age is: $ageAverage"
        textViewMedianAge.text="Median age is: $ageMedian"

    }

    //Get age from date of birth

    private fun getAge(year: Int, month: Int, day: Int): Int {
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()

        dob.set(year, month, day)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

        //if had birthday this year already

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--
        }

        val ageInt = age

        return ageInt
    }



    override fun onBackPressed() {
        finish()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
